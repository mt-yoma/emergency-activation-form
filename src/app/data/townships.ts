export const Townships = new Map([
    [
        1,
        [
            {
                "key": "AhGaYa",
                "value": "AhGaYa"
            },
            {
                "key": "BaMaNa",
                "value": "BaMaNa"
            },
            {
                "key": "DaPhaYa",
                "value": "DaPhaYa"
            },
            {
                "key": "HaPaNa",
                "value": "HaPaNa"
            },
            {
                "key": "HsaBaTa",
                "value": "HsaBaTa"
            },
            {
                "key": "HsaDaNa",
                "value": "HsaDaNa"
            },
            {
                "key": "HsaLaNa",
                "value": "HsaLaNa"
            },
            {
                "key": "HsaPaBa",
                "value": "HsaPaBa"
            },
            {
                "key": "KaMaNa",
                "value": "KaMaNa"
            },
            {
                "key": "KaMaTa",
                "value": "KaMaTa"
            },
            {
                "key": "KaPaTa",
                "value": "KaPaTa"
            },
            {
                "key": "KhaBaDa",
                "value": "KhaBaDa"
            },
            {
                "key": "KhaPhaNa",
                "value": "KhaPhaNa"
            },
            {
                "key": "LaGaNa",
                "value": "LaGaNa"
            },
            {
                "key": "MaKaNa",
                "value": "MaKaNa"
            },
            {
                "key": "MaKaTa",
                "value": "MaKaTa"
            },
            {
                "key": "MaKhaBa",
                "value": "MaKhaBa"
            },
            {
                "key": "MaLaNa",
                "value": "MaLaNa"
            },
            {
                "key": "MaMaNa",
                "value": "MaMaNa"
            },
            {
                "key": "MaNyaNa",
                "value": "MaNyaNa"
            },
            {
                "key": "MaSaNa",
                "value": "MaSaNa"
            },
            {
                "key": "NaMaNa",
                "value": "NaMaNa"
            },
            {
                "key": "PaNaDa",
                "value": "PaNaDa"
            },
            {
                "key": "PaTaAh",
                "value": "PaTaAh"
            },
            {
                "key": "PaWaNa",
                "value": "PaWaNa"
            },
            {
                "key": "TaNaNa",
                "value": "TaNaNa"
            },
            {
                "key": "WaMaNa",
                "value": "WaMaNa"
            },
            {
                "key": "YaBaYa",
                "value": "YaBaYa"
            },
            {
                "key": "YaKaNa",
                "value": "YaKaNa"
            }
        ]
    ],
    [
        2,
        [
            {
                "key": "BaLaKha",
                "value": "BaLaKha"
            },
            {
                "key": "DaMaHsa",
                "value": "DaMaHsa"
            },
            {
                "key": "LaKaNa",
                "value": "LaKaNa"
            },
            {
                "key": "MaSaNa",
                "value": "MaSaNa"
            },
            {
                "key": "PhaHsaNa",
                "value": "PhaHsaNa"
            },
            {
                "key": "PhaYaHsa",
                "value": "PhaYaHsa"
            },
            {
                "key": "YaTaNa",
                "value": "YaTaNa"
            },
            {
                "key": "YaThaNa",
                "value": "YaThaNa"
            }
        ]
    ],
    [
        3,
        [
            {
                "key": "BaAhNa",
                "value": "BaAhNa"
            },
            {
                "key": "BaGaLa",
                "value": "BaGaLa"
            },
            {
                "key": "BaThaHsa",
                "value": "BaThaHsa"
            },
            {
                "key": "KaDaNa",
                "value": "KaDaNa"
            },
            {
                "key": "KaHsaKa",
                "value": "KaHsaKa"
            },
            {
                "key": "KaKaYa",
                "value": "KaKaYa"
            },
            {
                "key": "KaMaMa",
                "value": "KaMaMa"
            },
            {
                "key": "LaBaNa",
                "value": "LaBaNa"
            },
            {
                "key": "LaThaNa",
                "value": "LaThaNa"
            },
            {
                "key": "MaWaTa",
                "value": "MaWaTa"
            },
            {
                "key": "PaKaNa",
                "value": "PaKaNa"
            },
            {
                "key": "PhaPaNa",
                "value": "PhaPaNa"
            },
            {
                "key": "SaKaLa",
                "value": "SaKaLa"
            },
            {
                "key": "ThaTaKa",
                "value": "ThaTaKa"
            },
            {
                "key": "WaLaMa",
                "value": "WaLaMa"
            },
            {
                "key": "YaYaTha",
                "value": "YaYaTha"
            }
        ]
    ],
    [
        4,
        [
            {
                "key": "HaKhaNa",
                "value": "HaKhaNa"
            },
            {
                "key": "HsaMaNa",
                "value": "HsaMaNa"
            },
            {
                "key": "KaKhaNa",
                "value": "KaKhaNa"
            },
            {
                "key": "KaPaLa",
                "value": "KaPaLa"
            },
            {
                "key": "MaTaNa",
                "value": "MaTaNa"
            },
            {
                "key": "MaTaPa",
                "value": "MaTaPa"
            },
            {
                "key": "PaLaWa",
                "value": "PaLaWa"
            },
            {
                "key": "PhaLaNa",
                "value": "PhaLaNa"
            },
            {
                "key": "TaTaLa",
                "value": "TaTaLa"
            },
            {
                "key": "TaTaNa",
                "value": "TaTaNa"
            },
            {
                "key": "TaZaNa",
                "value": "TaZaNa"
            },
            {
                "key": "YaKhaDa",
                "value": "YaKhaDa"
            },
            {
                "key": "YaZaNa",
                "value": "YaZaNa"
            }
        ]
    ],
    [
        5,
        [
            {
                "key": "AhTaNa",
                "value": "AhTaNa"
            },
            {
                "key": "AhYaTa",
                "value": "AhYaTa"
            },
            {
                "key": "BaMaNa",
                "value": "BaMaNa"
            },
            {
                "key": "BaTaLa",
                "value": "BaTaLa"
            },
            {
                "key": "DaHaNa",
                "value": "DaHaNa"
            },
            {
                "key": "DaPaYa",
                "value": "DaPaYa"
            },
            {
                "key": "HaMaLa",
                "value": "HaMaLa"
            },
            {
                "key": "HsaLaKa",
                "value": "HsaLaKa"
            },
            {
                "key": "HsaMaYa",
                "value": "HsaMaYa"
            },
            {
                "key": "HtaKhaNa",
                "value": "HtaKhaNa"
            },
            {
                "key": "HtaPaKha",
                "value": "HtaPaKha"
            },
            {
                "key": "KaBaLa",
                "value": "KaBaLa"
            },
            {
                "key": "KaLaHta",
                "value": "KaLaHta"
            },
            {
                "key": "KaLaNa",
                "value": "KaLaNa"
            },
            {
                "key": "KaLaTa",
                "value": "KaLaTa"
            },
            {
                "key": "KaLaWa",
                "value": "KaLaWa"
            },
            {
                "key": "KaMaNa",
                "value": "KaMaNa"
            },
            {
                "key": "KaNaNa",
                "value": "KaNaNa"
            },
            {
                "key": "KaThaNa",
                "value": "KaThaNa"
            },
            {
                "key": "KhaAuNa",
                "value": "KhaAuNa"
            },
            {
                "key": "KhaAuTa",
                "value": "KhaAuTa"
            },
            {
                "key": "KhaPaNa",
                "value": "KhaPaNa"
            },
            {
                "key": "KhaTaNa",
                "value": "KhaTaNa"
            },
            {
                "key": "LaHaNa",
                "value": "LaHaNa"
            },
            {
                "key": "LaYaNa",
                "value": "LaYaNa"
            },
            {
                "key": "MaKaNa",
                "value": "MaKaNa"
            },
            {
                "key": "MaLaNa",
                "value": "MaLaNa"
            },
            {
                "key": "MaMaNa",
                "value": "MaMaNa"
            },
            {
                "key": "MaMaTa",
                "value": "MaMaTa"
            },
            {
                "key": "MaPaLa",
                "value": "MaPaLa"
            },
            {
                "key": "MaThaNa",
                "value": "MaThaNa"
            },
            {
                "key": "MaYaNa",
                "value": "MaYaNa"
            },
            {
                "key": "NaYaNa",
                "value": "NaYaNa"
            },
            {
                "key": "PaHsaNa",
                "value": "PaHsaNa"
            },
            {
                "key": "PaLaBa",
                "value": "PaLaBa"
            },
            {
                "key": "PaLaNa",
                "value": "PaLaNa"
            },
            {
                "key": "PhaPaNa",
                "value": "PhaPaNa"
            },
            {
                "key": "SaKaNa",
                "value": "SaKaNa"
            },
            {
                "key": "TaHsaNa",
                "value": "TaHsaNa"
            },
            {
                "key": "TaMaNa",
                "value": "TaMaNa"
            },
            {
                "key": "WaLaNa",
                "value": "WaLaNa"
            },
            {
                "key": "WaThaNa",
                "value": "WaThaNa"
            },
            {
                "key": "YaAuNa",
                "value": "YaAuNa"
            },
            {
                "key": "YaBaNa",
                "value": "YaBaNa"
            },
            {
                "key": "YaMaPa",
                "value": "YaMaPa"
            }
        ]
    ],
    [
        6,
        [
            {
                "key": "BaPaNa",
                "value": "BaPaNa"
            },
            {
                "key": "HtaWaNa",
                "value": "HtaWaNa"
            },
            {
                "key": "KaLaNa",
                "value": "KaLaNa"
            },
            {
                "key": "KaSaNa",
                "value": "KaSaNa"
            },
            {
                "key": "KaThaNa",
                "value": "KaThaNa"
            },
            {
                "key": "KaYaYa",
                "value": "KaYaYa"
            },
            {
                "key": "KhaMaKa",
                "value": "KhaMaKa"
            },
            {
                "key": "LaLaNa",
                "value": "LaLaNa"
            },
            {
                "key": "MaAhNa",
                "value": "MaAhNa"
            },
            {
                "key": "MaAhYa",
                "value": "MaAhYa"
            },
            {
                "key": "MaMaNa",
                "value": "MaMaNa"
            },
            {
                "key": "MaTaNa",
                "value": "MaTaNa"
            },
            {
                "key": "PaKaMa",
                "value": "PaKaMa"
            },
            {
                "key": "PaLaNa",
                "value": "PaLaNa"
            },
            {
                "key": "PaLaTa",
                "value": "PaLaTa"
            },
            {
                "key": "TaThaYa",
                "value": "TaThaYa"
            },
            {
                "key": "ThaYaKha",
                "value": "ThaYaKha"
            },
            {
                "key": "YaPhaNa",
                "value": "YaPhaNa"
            }
        ]
    ],
    [
        7,
        [
            {
                "key": "AhPhaNa",
                "value": "AhPhaNa"
            },
            {
                "key": "AhTaNa",
                "value": "AhTaNa"
            },
            {
                "key": "DaAuNa",
                "value": "DaAuNa"
            },
            {
                "key": "HtaTaPa",
                "value": "HtaTaPa"
            },
            {
                "key": "KaKaNa",
                "value": "KaKaNa"
            },
            {
                "key": "KaPaKa",
                "value": "KaPaKa"
            },
            {
                "key": "KaTaKha",
                "value": "KaTaKha"
            },
            {
                "key": "KaWaNa",
                "value": "KaWaNa"
            },
            {
                "key": "LaPaTa",
                "value": "LaPaTa"
            },
            {
                "key": "MaLaNa",
                "value": "MaLaNa"
            },
            {
                "key": "MaNyaNa",
                "value": "MaNyaNa"
            },
            {
                "key": "NaTaLa",
                "value": "NaTaLa"
            },
            {
                "key": "NyaLaPa",
                "value": "NyaLaPa"
            },
            {
                "key": "PaKhaNa",
                "value": "PaKhaNa"
            },
            {
                "key": "PaKhaTa",
                "value": "PaKhaTa"
            },
            {
                "key": "PaMaNa",
                "value": "PaMaNa"
            },
            {
                "key": "PaTaNa",
                "value": "PaTaNa"
            },
            {
                "key": "PaTaTa",
                "value": "PaTaTa"
            },
            {
                "key": "PhaMaNa",
                "value": "PhaMaNa"
            },
            {
                "key": "TaNgaNa",
                "value": "TaNgaNa"
            },
            {
                "key": "ThaKaNa",
                "value": "ThaKaNa"
            },
            {
                "key": "ThaNaPa",
                "value": "ThaNaPa"
            },
            {
                "key": "ThaWaTa",
                "value": "ThaWaTa"
            },
            {
                "key": "WaMaNa",
                "value": "WaMaNa"
            },
            {
                "key": "YaKaNa",
                "value": "YaKaNa"
            },
            {
                "key": "YaTaNa",
                "value": "YaTaNa"
            },
            {
                "key": "YaTaYa",
                "value": "YaTaYa"
            },
            {
                "key": "ZaKaNa",
                "value": "ZaKaNa"
            }
        ]
    ],
    [
        8,
        [
            {
                "key": "AhLaNa",
                "value": "AhLaNa"
            },
            {
                "key": "GaGaNa",
                "value": "GaGaNa"
            },
            {
                "key": "HsaMaNa",
                "value": "HsaMaNa"
            },
            {
                "key": "HsaPaWa",
                "value": "HsaPaWa"
            },
            {
                "key": "HsaPhaNa",
                "value": "HsaPhaNa"
            },
            {
                "key": "HtaLaNa",
                "value": "HtaLaNa"
            },
            {
                "key": "KaHtaNa",
                "value": "KaHtaNa"
            },
            {
                "key": "KaMaNa",
                "value": "KaMaNa"
            },
            {
                "key": "KhaMaNa",
                "value": "KhaMaNa"
            },
            {
                "key": "MaBaNa",
                "value": "MaBaNa"
            },
            {
                "key": "MaHtaNa",
                "value": "MaHtaNa"
            },
            {
                "key": "MaKaNa",
                "value": "MaKaNa"
            },
            {
                "key": "MaLaNa",
                "value": "MaLaNa"
            },
            {
                "key": "MaMaNa",
                "value": "MaMaNa"
            },
            {
                "key": "MaTaNa",
                "value": "MaTaNa"
            },
            {
                "key": "MaThaNa",
                "value": "MaThaNa"
            },
            {
                "key": "NaMaNa",
                "value": "NaMaNa"
            },
            {
                "key": "NgaPhaNa",
                "value": "NgaPhaNa"
            },
            {
                "key": "PaKhaKa",
                "value": "PaKhaKa"
            },
            {
                "key": "PaMaNa",
                "value": "PaMaNa"
            },
            {
                "key": "PaPhaNa",
                "value": "PaPhaNa"
            },
            {
                "key": "SaLaNa",
                "value": "SaLaNa"
            },
            {
                "key": "SaTaYa",
                "value": "SaTaYa"
            },
            {
                "key": "TaTaKa",
                "value": "TaTaKa"
            },
            {
                "key": "ThaYaNa",
                "value": "ThaYaNa"
            },
            {
                "key": "YaNaKha",
                "value": "YaNaKha"
            },
            {
                "key": "YaSaKa",
                "value": "YaSaKa"
            }
        ],
    ],
    [
        9,
        [
            {
                "key": "AhMaYa",
                "value": "AhMaYa"
            },
            {
                "key": "AhMaZa",
                "value": "AhMaZa"
            },
            {
                "key": "AuTaTha",
                "value": "AuTaTha"
            },
            {
                "key": "DaKhaTha",
                "value": "DaKhaTha"
            },
            {
                "key": "KaHsaNa",
                "value": "KaHsaNa"
            },
            {
                "key": "KaPaTa",
                "value": "KaPaTa"
            },
            {
                "key": "KhaAhZa",
                "value": "KhaAhZa"
            },
            {
                "key": "KhaMaSa",
                "value": "KhaMaSa"
            },
            {
                "key": "LaWaNa",
                "value": "LaWaNa"
            },
            {
                "key": "MaHaMa",
                "value": "MaHaMa"
            },
            {
                "key": "MaHtaLa",
                "value": "MaHtaLa"
            },
            {
                "key": "MaKaNa",
                "value": "MaKaNa"
            },
            {
                "key": "MaKhaNa",
                "value": "MaKhaNa"
            },
            {
                "key": "MaLaNa",
                "value": "MaLaNa"
            },
            {
                "key": "MaMaNa",
                "value": "MaMaNa"
            },
            {
                "key": "MaNaMa",
                "value": "MaNaMa"
            },
            {
                "key": "MaNaTa",
                "value": "MaNaTa"
            },
            {
                "key": "MaTaYa",
                "value": "MaTaYa"
            },
            {
                "key": "MaThaNa",
                "value": "MaThaNa"
            },
            {
                "key": "MaYaMa",
                "value": "MaYaMa"
            },
            {
                "key": "MaYaTa",
                "value": "MaYaTa"
            },
            {
                "key": "NaHtaKa",
                "value": "NaHtaKa"
            },
            {
                "key": "NgaThaYa",
                "value": "NgaThaYa"
            },
            {
                "key": "NgaZaNa",
                "value": "NgaZaNa"
            },
            {
                "key": "NyaAuNa",
                "value": "NyaAuNa"
            },
            {
                "key": "PaAuLa",
                "value": "PaAuLa"
            },
            {
                "key": "PaBaNa",
                "value": "PaBaNa"
            },
            {
                "key": "PaBaTha",
                "value": "PaBaTha"
            },
            {
                "key": "PaKaKha",
                "value": "PaKaKha"
            },
            {
                "key": "PaMaNa",
                "value": "PaMaNa"
            },
            {
                "key": "PaThaKa",
                "value": "PaThaKa"
            },
            {
                "key": "SaKaNa",
                "value": "SaKaNa"
            },
            {
                "key": "SaKaTa",
                "value": "SaKaTa"
            },
            {
                "key": "TaKaNa",
                "value": "TaKaNa"
            },
            {
                "key": "TaKaTa",
                "value": "TaKaTa"
            },
            {
                "key": "TaTaAu",
                "value": "TaTaAu"
            },
            {
                "key": "TaThaNa",
                "value": "TaThaNa"
            },
            {
                "key": "ThaPaKa",
                "value": "ThaPaKa"
            },
            {
                "key": "ThaSaNa",
                "value": "ThaSaNa"
            },
            {
                "key": "WaTaNa",
                "value": "WaTaNa"
            },
            {
                "key": "YaMaTha",
                "value": "YaMaTha"
            },
            {
                "key": "ZaMaTha",
                "value": "ZaMaTha"
            },
            {
                "key": "ZaYaTha",
                "value": "ZaYaTha"
            }
        ]
    ],
    [
        10,
        [
            {
                "key": "BaLaNa",
                "value": "BaLaNa"
            },
            {
                "key": "KaHtaNa",
                "value": "KaHtaNa"
            },
            {
                "key": "KaMaYa",
                "value": "KaMaYa"
            },
            {
                "key": "KhaHsaNa",
                "value": "KhaHsaNa"
            },
            {
                "key": "KhaZaNa",
                "value": "KhaZaNa"
            },
            {
                "key": "LaMaNa",
                "value": "LaMaNa"
            },
            {
                "key": "MaDaNa",
                "value": "MaDaNa"
            },
            {
                "key": "MaLaMa",
                "value": "MaLaMa"
            },
            {
                "key": "PaMaNa",
                "value": "PaMaNa"
            },
            {
                "key": "ThaHtaNa",
                "value": "ThaHtaNa"
            },
            {
                "key": "ThaPhaYa",
                "value": "ThaPhaYa"
            },
            {
                "key": "YaMaNa",
                "value": "YaMaNa"
            }
        ]
    ],
    [
        11,
        [
            {
                "key": "AhMaNa",
                "value": "AhMaNa"
            },
            {
                "key": "BaThaTa",
                "value": "BaThaTa"
            },
            {
                "key": "GaMaNa",
                "value": "GaMaNa"
            },
            {
                "key": "KaNaLa",
                "value": "KaNaLa"
            },
            {
                "key": "KaPhaNa",
                "value": "KaPhaNa"
            },
            {
                "key": "KaTaNa",
                "value": "KaTaNa"
            },
            {
                "key": "MaAhNa",
                "value": "MaAhNa"
            },
            {
                "key": "MaAhTa",
                "value": "MaAhTa"
            },
            {
                "key": "MaAuNa",
                "value": "MaAuNa"
            },
            {
                "key": "MaPaNa",
                "value": "MaPaNa"
            },
            {
                "key": "MaPaTa",
                "value": "MaPaTa"
            },
            {
                "key": "MaTaNa",
                "value": "MaTaNa"
            },
            {
                "key": "PaNaKa",
                "value": "PaNaKa"
            },
            {
                "key": "PaTaNa",
                "value": "PaTaNa"
            },
            {
                "key": "SaTaNa",
                "value": "SaTaNa"
            },
            {
                "key": "TaKaNa",
                "value": "TaKaNa"
            },
            {
                "key": "TaPaWa",
                "value": "TaPaWa"
            },
            {
                "key": "ThaTaNa",
                "value": "ThaTaNa"
            },
            {
                "key": "YaBaNa",
                "value": "YaBaNa"
            },
            {
                "key": "YaThaTa",
                "value": "YaThaTa"
            }
        ]
    ],
    [
        12,
        [
            {
                "key": "AhLaNa",
                "value": "AhLaNa"
            },
            {
                "key": "AhSaNa",
                "value": "AhSaNa"
            },
            {
                "key": "AuKaMa",
                "value": "AuKaMa"
            },
            {
                "key": "AuKaTa",
                "value": "AuKaTa"
            },
            {
                "key": "BaHaNa",
                "value": "BaHaNa"
            },
            {
                "key": "BaTaHta",
                "value": "BaTaHta"
            },
            {
                "key": "DaGaHsa",
                "value": "DaGaHsa"
            },
            {
                "key": "DaGaMa",
                "value": "DaGaMa"
            },
            {
                "key": "DaGaNa",
                "value": "DaGaNa"
            },
            {
                "key": "DaGaTa",
                "value": "DaGaTa"
            },
            {
                "key": "DaGaYa",
                "value": "DaGaYa"
            },
            {
                "key": "DaLaNa",
                "value": "DaLaNa"
            },
            {
                "key": "DaPaNa",
                "value": "DaPaNa"
            },
            {
                "key": "HsaKaKha",
                "value": "HsaKaKha"
            },
            {
                "key": "HsaKaNa",
                "value": "HsaKaNa"
            },
            {
                "key": "HtaTaPa",
                "value": "HtaTaPa"
            },
            {
                "key": "KaKaKa",
                "value": "KaKaKa"
            },
            {
                "key": "KaKhaKa",
                "value": "KaKhaKa"
            },
            {
                "key": "KaMaNa",
                "value": "KaMaNa"
            },
            {
                "key": "KaMaTa",
                "value": "KaMaTa"
            },
            {
                "key": "KaMaYa",
                "value": "KaMaYa"
            },
            {
                "key": "KaTaNa",
                "value": "KaTaNa"
            },
            {
                "key": "KaTaTa",
                "value": "KaTaTa"
            },
            {
                "key": "KhaYaNa",
                "value": "KhaYaNa"
            },
            {
                "key": "LaKaNa",
                "value": "LaKaNa"
            },
            {
                "key": "LaMaNa",
                "value": "LaMaNa"
            },
            {
                "key": "LaMaTa",
                "value": "LaMaTa"
            },
            {
                "key": "LaThaNa",
                "value": "LaThaNa"
            },
            {
                "key": "LaThaYa",
                "value": "LaThaYa"
            },
            {
                "key": "MaBaNa",
                "value": "MaBaNa"
            },
            {
                "key": "MaGaDa",
                "value": "MaGaDa"
            },
            {
                "key": "MaGaTa",
                "value": "MaGaTa"
            },
            {
                "key": "MaYaKa",
                "value": "MaYaKa"
            },
            {
                "key": "PaBaTa",
                "value": "PaBaTa"
            },
            {
                "key": "PaZaTa",
                "value": "PaZaTa"
            },
            {
                "key": "SaKhaNa",
                "value": "SaKhaNa"
            },
            {
                "key": "TaKaNa",
                "value": "TaKaNa"
            },
            {
                "key": "TaMaNa",
                "value": "TaMaNa"
            },
            {
                "key": "TaTaNa",
                "value": "TaTaNa"
            },
            {
                "key": "TaTaTa",
                "value": "TaTaTa"
            },
            {
                "key": "ThaGaKa",
                "value": "ThaGaKa"
            },
            {
                "key": "ThaKaTa",
                "value": "ThaKaTa"
            },
            {
                "key": "ThaKhaNa",
                "value": "ThaKhaNa"
            },
            {
                "key": "ThaLaNa",
                "value": "ThaLaNa"
            },
            {
                "key": "YaKaNa",
                "value": "YaKaNa"
            },
            {
                "key": "YaPaTha",
                "value": "YaPaTha"
            }
        ]
    ],
    [
        13,
        [
            {
                "key": "AhTaNa",
                "value": "AhTaNa"
            },
            {
                "key": "HaMaNa",
                "value": "HaMaNa"
            },
            {
                "key": "HaPaNa",
                "value": "HaPaNa"
            },
            {
                "key": "HaPaTa",
                "value": "HaPaTa"
            },
            {
                "key": "HsaHsaNa",
                "value": "HsaHsaNa"
            },
            {
                "key": "KaHaNa",
                "value": "KaHaNa"
            },
            {
                "key": "KaKaNa",
                "value": "KaKaNa"
            },
            {
                "key": "KaKhaNa",
                "value": "KaKhaNa"
            },
            {
                "key": "KaLaDa",
                "value": "KaLaDa"
            },
            {
                "key": "KaLaHta",
                "value": "KaLaHta"
            },
            {
                "key": "KaLaNa",
                "value": "KaLaNa"
            },
            {
                "key": "KaLaTa",
                "value": "KaLaTa"
            },
            {
                "key": "KaMaNa",
                "value": "KaMaNa"
            },
            {
                "key": "KaTaLa",
                "value": "KaTaLa"
            },
            {
                "key": "KaTaNa",
                "value": "KaTaNa"
            },
            {
                "key": "KaTaTa",
                "value": "KaTaTa"
            },
            {
                "key": "KaThaNa",
                "value": "KaThaNa"
            },
            {
                "key": "KhaLaNa",
                "value": "KhaLaNa"
            },
            {
                "key": "KhaYaHa",
                "value": "KhaYaHa"
            },
            {
                "key": "LaKaNa",
                "value": "LaKaNa"
            },
            {
                "key": "LaKhaNa",
                "value": "LaKhaNa"
            },
            {
                "key": "LaKhaTa",
                "value": "LaKhaTa"
            },
            {
                "key": "LaLaNa",
                "value": "LaLaNa"
            },
            {
                "key": "LaYaNa",
                "value": "LaYaNa"
            },
            {
                "key": "MaBaNa",
                "value": "MaBaNa"
            },
            {
                "key": "MaHaYa",
                "value": "MaHaYa"
            },
            {
                "key": "MaHsaNa",
                "value": "MaHsaNa"
            },
            {
                "key": "MaHsaTa",
                "value": "MaHsaTa"
            },
            {
                "key": "MaHtaNa",
                "value": "MaHtaNa"
            },
            {
                "key": "MaHtaTa",
                "value": "MaHtaTa"
            },
            {
                "key": "MaKaHta",
                "value": "MaKaHta "
            },
            {
                "key": "MaKaNa",
                "value": "MaKaNa"
            },
            {
                "key": "MaKaTa",
                "value": "MaKaTa"
            },
            {
                "key": "MaKhaNa",
                "value": "MaKhaNa"
            },
            {
                "key": "MaKhaTa",
                "value": "MaKhaTa"
            },
            {
                "key": "MaLaNa",
                "value": "MaLaNa"
            },
            {
                "key": "MaLaTa",
                "value": "MaLaTa"
            },
            {
                "key": "MaMaHta",
                "value": "MaMaHta"
            },
            {
                "key": "MaMaNa",
                "value": "MaMaNa"
            },
            {
                "key": "MaMaTa",
                "value": "MaMaTa"
            },
            {
                "key": "MaNaNa",
                "value": "MaNaNa"
            },
            {
                "key": "MaNaTa",
                "value": "MaNaTa"
            },
            {
                "key": "MaNgaNa",
                "value": "MaNgaNa"
            },
            {
                "key": "MaPaHta",
                "value": "MaPaHta"
            },
            {
                "key": "MaPaNa",
                "value": "MaPaNa"
            },
            {
                "key": "MaPaTa",
                "value": "MaPaTa"
            },
            {
                "key": "MaPhaNa",
                "value": "MaPhaNa"
            },
            {
                "key": "MaSaNa",
                "value": "MaSaNa"
            },
            {
                "key": "MaTaNa",
                "value": "MaTaNa"
            },
            {
                "key": "MaTaNa",
                "value": "MaTaNa"
            },
            {
                "key": "MaYaHta",
                "value": "MaYaHta"
            },
            {
                "key": "MaYaNa",
                "value": "MaYaNa"
            },
            {
                "key": "MaYaNa",
                "value": "MaYaNa"
            },
            {
                "key": "MaYaTa",
                "value": "MaYaTa"
            },
            {
                "key": "MaYaTa",
                "value": "MaYaTa"
            },
            {
                "key": "NaHsaNa",
                "value": "NaHsaNa"
            },
            {
                "key": "NaKhaNa",
                "value": "NaKhaNa"
            },
            {
                "key": "NaKhaNa",
                "value": "NaKhaNa"
            },
            {
                "key": "NaKhaTa",
                "value": "NaKhaTa"
            },
            {
                "key": "NaMaTa",
                "value": "NaMaTa"
            },
            {
                "key": "NaPhaNa",
                "value": "NaPhaNa"
            },
            {
                "key": "NaSaNa",
                "value": "NaSaNa"
            },
            {
                "key": "NaTaNa",
                "value": "NaTaNa"
            },
            {
                "key": "NaTaYa",
                "value": "NaTaYa"
            },
            {
                "key": "NyaYaNa",
                "value": "NyaYaNa"
            },
            {
                "key": "PaHsaNa",
                "value": "PaHsaNa"
            },
            {
                "key": "PaHsaNa",
                "value": "PaHsaNa"
            },
            {
                "key": "PaLaHta",
                "value": "PaLaHta"
            },
            {
                "key": "PaLaNa",
                "value": "PaLaNa"
            },
            {
                "key": "PaLaTa",
                "value": "PaLaTa"
            },
            {
                "key": "PaPaKa",
                "value": "PaPaKa"
            },
            {
                "key": "PaTaYa",
                "value": "PaTaYa"
            },
            {
                "key": "PaWaNa",
                "value": "PaWaNa"
            },
            {
                "key": "PaYaNa",
                "value": "PaYaNa"
            },
            {
                "key": "PhaKhaNa",
                "value": "PhaKhaNa"
            },
            {
                "key": "TaKaNa",
                "value": "TaKaNa"
            },
            {
                "key": "TaKhaLa",
                "value": "TaKhaLa"
            },
            {
                "key": "TaLaNa",
                "value": "TaLaNa"
            },
            {
                "key": "TaMaNya",
                "value": "TaMaNya"
            },
            {
                "key": "TaTaNa",
                "value": "TaTaNa"
            },
            {
                "key": "TaYaNa",
                "value": "TaYaNa"
            },
            {
                "key": "ThaNaNa",
                "value": "ThaNaNa"
            },
            {
                "key": "ThaPaNa",
                "value": "ThaPaNa"
            },
            {
                "key": "YaNgaNa",
                "value": "YaNgaNa"
            },
            {
                "key": "YaSaNa",
                "value": "YaSaNa"
            }
        ]
    ],
    [
        14,
        [
            {
                "key": "AhGaPa",
                "value": "AhGaPa"
            },
            {
                "key": "AhMaNa",
                "value": "AhMaNa"
            },
            {
                "key": "AhMaNa",
                "value": "AhMaNa"
            },
            {
                "key": "AhMaTa",
                "value": "AhMaTa"
            },
            {
                "key": "BaKaLa",
                "value": "BaKaLa"
            },
            {
                "key": "DaDaYa",
                "value": "DaDaYa"
            },
            {
                "key": "DaNaPha",
                "value": "DaNaPha"
            },
            {
                "key": "HaKaKa",
                "value": "HaKaKa"
            },
            {
                "key": "HaThaTa",
                "value": "HaThaTa"
            },
            {
                "key": "KaKaHta",
                "value": "KaKaHta"
            },
            {
                "key": "KaKaNa",
                "value": "KaKaNa"
            },
            {
                "key": "KaKhaNa",
                "value": "KaKhaNa"
            },
            {
                "key": "KaLaNa",
                "value": "KaLaNa"
            },
            {
                "key": "KaPaNa",
                "value": "KaPaNa"
            },
            {
                "key": "LaMaNa",
                "value": "LaMaNa"
            },
            {
                "key": "LaPaTa",
                "value": "LaPaTa"
            },
            {
                "key": "MaAhNa",
                "value": "MaAhNa"
            },
            {
                "key": "MaAhPa",
                "value": "MaAhPa"
            },
            {
                "key": "MaMaKa",
                "value": "MaMaKa"
            },
            {
                "key": "MaMaNa",
                "value": "MaMaNa"
            },
            {
                "key": "NgaHsaNa",
                "value": "NgaHsaNa"
            },
            {
                "key": "NgaPaTa",
                "value": "NgaPaTa"
            },
            {
                "key": "NgaThaKha",
                "value": "NgaThaKha"
            },
            {
                "key": "NgaYaKa",
                "value": "NgaYaKa"
            },
            {
                "key": "NyaTaNa",
                "value": "NyaTaNa"
            },
            {
                "key": "PaSaLa",
                "value": "PaSaLa"
            },
            {
                "key": "PaTaNa",
                "value": "PaTaNa"
            },
            {
                "key": "PaThaNa",
                "value": "PaThaNa"
            },
            {
                "key": "PhaPaNa",
                "value": "PhaPaNa"
            },
            {
                "key": "ThaPaNa",
                "value": "ThaPaNa"
            },
            {
                "key": "WaKhaMa",
                "value": "WaKhaMa"
            },
            {
                "key": "YaKaNa",
                "value": "YaKaNa"
            },
            {
                "key": "YaThaYa",
                "value": "YaThaYa"
            },
            {
                "key": "ZaLaNa",
                "value": "ZaLaNa"
            }
        ]
    ]
]);