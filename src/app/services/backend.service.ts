import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  constructor(private http: HttpClient) { }

  postUserInfo(user: User): Observable<any> {
    const dob = new Date(user.dob);
    const modifiedDob = `${dob.getDate()}/${dob.getMonth() + 1}/${dob.getFullYear()}`;
    user.dob = modifiedDob;
    return this.http.post("https://dev-otp-request.yomabank.org/api/v1/user-info", user);
  }

}
