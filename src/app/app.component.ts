import { Component, OnInit } from '@angular/core';
import { BackendService } from './services/backend.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RecaptchaErrorParameters } from "ng-recaptcha";
import { User } from './models/user';
import { Townships } from './data/townships';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'activation-request-form';
  maxDate: Date = new Date('12/31/2003');
  isSuccess: boolean = false;
  captchaResponse: string = "";
  isLoading: boolean = false;
  userForm = new FormGroup({
    customerName: new FormControl('', [Validators.required]),
    mobileNo: new FormControl('', [Validators.required, Validators.pattern('^(09)([0-9]{7,9})$')]),
    nrcRegion: new FormControl(12, [Validators.required]),
    nrcTownship: new FormControl('AhLaNa', [Validators.required]),
    nrcCitizen: new FormControl('(N)', [Validators.required]),
    nrcNumber: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{6}$')]),
    dob: new FormControl('', [Validators.required]),
    atmCardNo: new FormControl('', [Validators.required, Validators.pattern('^(95)[0-9]{14}$')]),
  });

  townships: any[] = Townships.get(12);

  get Citizens(): string[] {
    return ["(N)", "(E)", "(P)", "(Tha)", "(Thi)"];
  }

  constructor(private backendService: BackendService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }

  get form() {
    return this.userForm.controls;
  }

  submitUserInfo() {
    this.isLoading = true;
    this.userForm.disable();
    const formattedNrc = `${this.userForm.get('nrcRegion').value}/${this.userForm.get('nrcTownship').value}${this.userForm.get('nrcCitizen').value}${this.userForm.get('nrcNumber').value}`;
    const info = new User();
    info.customerName = this.userForm.get('customerName').value;
    info.mobileNo = this.userForm.get('mobileNo').value;
    info.nrc = formattedNrc;
    info.nrcNo = this.userForm.get('nrcNumber').value;
    info.dob = this.userForm.get('dob').value;
    info.atmCardNo = this.userForm.get('atmCardNo').value;
    this.backendService.postUserInfo(info).subscribe((response) => {
      this.userForm.reset();
      this.isSuccess = true;
      this.isLoading = false;
      this.userForm.enable();
    }, error => {
      console.log(error);
      this.isLoading = false;
      this.userForm.enable();
      this.snackBar.open('Something went wrong!', 'OK', {
        duration: 5000,
      });
    })
  }

  public changedTownship($evt) {
    const townshipCodes = Townships.get($evt.value);
    if (!townshipCodes?.length) {
      this.snackBar.open('NRC service error!', 'OK', {
        duration: 3000,
      });
      return;
    }
    this.townships = townshipCodes;
    this.userForm.get('nrcTownship').setValue(this.townships[0].value);
  }

  public resolved(captchaResponse: string): void {
    const newResponse = captchaResponse
      ? `${captchaResponse.substr(0, 7)}...${captchaResponse.substr(-7)}`
      : captchaResponse;
    this.captchaResponse += `${JSON.stringify(newResponse)}\n`;
    this.submitUserInfo();
  }

  public onError(errorDetails: RecaptchaErrorParameters): void {
    this.captchaResponse += `ERROR; error details (if any) have been logged to console\n`;
    console.log(`reCAPTCHA error encountered; details:`, errorDetails);
  }
}
