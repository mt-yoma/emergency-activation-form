export class User {
    customerName: string;
    mobileNo: string;
    nrc: string;
    nrcNo: string;
    dob: string;
    atmCardNo: string;
}